public class task3 {
    public static void main(String[] args) {

        displayMemoryUsage("Memori Awal : ");
        // Alokasi memori dinamis dengan membuat objek baru
        for (int i = 0; i < 5; i++) {
            allocateMemory();
        }

        displayMemoryUsage("Memori setelah membuat object : ");
    }

    private static void allocateMemory() {
        // Alokasi memori dengan membuat objek baru
        Object obj = new Object();

        // Tampilkan informasi objek
        System.out.println("Objek baru dibuat: " + obj);

        // Gantilah objek dengan null untuk menunjukkan bahwa objek tidak lagi direferensi
        obj = null;

        System.gc();
    }

    private static void displayMemoryUsage(String message) {
        // Get the current runtime
        Runtime runtime = Runtime.getRuntime();

        // Calculate used memory in bytes
        long usedMemory = runtime.totalMemory() - runtime.freeMemory();

        // Display the used memory
        System.out.println(message + usedMemory + " bytes");
    }
}
