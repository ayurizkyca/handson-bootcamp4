import java.util.Scanner;

public class StringKata {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userInput;

        System.out.print("Ketik Kalimat, lalu tekan enter : ");
        userInput = scanner.nextLine();

        garis();
        System.out.println("String penuh: " + userInput);
        garis();
        System.out.println("Proses Memecahkan kata/kalimat menjadi karakter.....Done");
        garis();

        // Menampilkan setiap karakter secara terpisah
        for (int i = 0; i < userInput.length(); i++) {
            char character = userInput.charAt(i);
            System.out.println("Karakter [" + (i+1) + "] : " + character);
        }
        garis();
        scanner.close();        
    }

    static void garis(){
        System.out.println("================================");
    }
}